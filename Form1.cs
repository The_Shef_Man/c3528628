﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c3528628
{
    public partial class Form1 : Form
    {
        CLIProcessing CLProcessor = new CLIProcessing();

        public Form1()
        {
            InitializeComponent();

            CLProcessor.setConsole(richTextBox2);
            CLProcessor.setGraphics(pictureBox1);
            //Initializes the system by passing component information required for console reporting or drawing output
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        /*
         * Command to handle CLI program running
         */
        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String[] commandArray = richTextBox1.Lines;
                int arrLen = commandArray.Length;
                String lastLine = commandArray[arrLen - 1].ToLower();
                String[] stringSeparators = new String[] { " " };
                if (lastLine == "run")
                {
                    //on detection of a press of the enter key by the user whilst in the CLI the system
                    //converts all the lines in the CLI to a set of arrayed strings, which are then checked to see
                    //if the final line is the run command at which point the program runs all the commands
                    for (int i = 0; i < arrLen - 1; i++)
                    {
                        String[] currentCommand = commandArray[i].Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                        CLProcessor.runCommand(currentCommand, i);
                        if (CLProcessor.getLineUpdate())
                        {
                            i = CLProcessor.getLastWhile() -1;
                        }
                    }
                }
            }
        }
        /*
         * Clears the console on double click of the console text box
         */
        private void richTextBox2_DoubleClick(object sender, EventArgs e)
        {
            richTextBox2.Clear();
        }
        /*
         * Clears the command box on double click of the text area
         */
        private void richTextBox1_DoubleClick(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }
        /*
         * Handles single commands in the standard text box
         */
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //Whenever the system receives a keyboard input it verifies whether or not it is a
                //press of the enter key
                String command = textBox1.Text;
                if (command == "run")
                {
                    richTextBox2.AppendText("Line 0: Run is not a valid instant command\n");
                }
                else 
                {
                    String[] stringSeparators = new String[] { " " };
                    String[] currentCommand = command.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    CLProcessor.runCommand(currentCommand, 0);
                }
                //Allows the system to handle to processing of the single command, with a specifc
                //stipulation on the run command which is not required for single cmmands in the
                //single run box, erroring out as required
                textBox1.Clear();
            }
        }
        /*
         * Handles on Click command for the save to file button
         */
        private void button1_Click(object sender, EventArgs e)
        { 
            MemoryStream userInput = new MemoryStream();
            richTextBox1.SaveFile(userInput, RichTextBoxStreamType.PlainText);
            //Creates a memory stream and stores the contents of the command interface to it for file writing
            SaveFileDialog commSave = new SaveFileDialog();
            commSave.OverwritePrompt = true;
            commSave.FileName = "myCommands";
            commSave.DefaultExt = "txt";
            commSave.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            commSave.Title = "Save Commands to Text File";
            commSave.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //Create the details and initial state of the file save dialog
            DialogResult result = commSave.ShowDialog();
            Stream fileStream;
            if(result == DialogResult.OK)
            {
                fileStream = commSave.OpenFile();
                userInput.Position = 0;
                userInput.WriteTo(fileStream);
                fileStream.Close();
            }
            //After checking the saveDialog can open the system creates and writes a text file using the saved
            //contents of the command box
        }
        /*
         * Handles on Click command for the Load file button
         */
        private void button2_Click(object sender, EventArgs e)
        { 
            OpenFileDialog openCommands = new OpenFileDialog();
            openCommands.DefaultExt = "*.txt";
            openCommands.Filter = "Text files (*.txt)|*.txt";
            openCommands.Title = "Open Command File";
            openCommands.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //Creates an Open file dialog and creates default parameters i.e. what files it is allowed to read
            if (openCommands.ShowDialog() == DialogResult.OK && openCommands.FileName.Length > 0)
            {
                var fileStream = openCommands.OpenFile();
                using (StreamReader rdr = new StreamReader(fileStream))
                {
                    String filecontent = rdr.ReadToEnd();
                    richTextBox1.Clear();
                    richTextBox1.AppendText(filecontent);
                }
                // Ensures the program can open the dialog box and the text file the user is trying to retrive is valid
                // the system then converts the file contents to a string to input into the command area
            }
        }
    }
}
