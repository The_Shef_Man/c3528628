﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime;

namespace c3528628
{
    class mathHandler
    {
        List<String> varNames = new List<String>();
        List<String> varValues = new List<String>();
        public bool paramCheck(String canParse)
        {
            bool booVal = false;
            booVal = intCheck(canParse);
            if (!booVal)
            {
                booVal = varCheck(canParse);
            }
            return booVal;
        }
        public bool intCheck(String canParse)
        {
            int chkr;
            bool booVal = int.TryParse(canParse, out chkr);
            return booVal;
        }
        public bool varCheck(String isVar)
        {
            bool matchfound = false;
            if (varNames.Contains(isVar))
            {
                matchfound = true;
            }
            return matchfound;
        }
        public int getIndex(String varName)
        {
            return varNames.IndexOf(varName);
        }
        public void setVar(String varName, String varVal)
        {
            if (varCheck(varName))
            {
                int indexer = getIndex(varName);
                if (varCheck(varVal))
                {
                    varValues.Insert(indexer, getVarVal(varVal).ToString());
                }
                else
                {
                    varValues.Insert(indexer, varVal);
                }
                
            }
            else
            {
                varNames.Add(varName);
                if (varCheck(varVal))
                {
                    varValues.Add(getVarVal(varVal).ToString());
                }
                else { 
                    varValues.Add(varVal);
                }
                
            }
        }
        public int getVarVal(String varName)
        {
            int indexer = getIndex(varName);
            string inter = varValues[indexer];
            return int.Parse(inter);
        }
        public int paramHandle(String param)
        {
            int res;
            bool isInt = intCheck(param);
            if (isInt)
            {
                res = int.Parse(param);
            }
            else
            {
                res = getVarVal(param);
            }
            return res;
        }
        public string MathEval(String equation)
        {
            DataTable dt = new DataTable();
            int varnumber = varNames.Count;
            for(int i = 0; i< varnumber; i++)
            {
                if (equation.Contains(varNames[i]))
                {
                    equation = equation.Replace(varNames[i], varValues[i]);
                }
            }
            double dbl = Convert.ToDouble(dt.Compute(equation, ""));
            dbl = Math.Floor(dbl);
            string inter = dbl.ToString();
            return inter;
        }
        public bool operation(String operands)
        {
            DataTable dt = new DataTable();
            int varnumber = varNames.Count;
            string[] operationR = { "<", ">", "==", "!=", "<=", ">=" };
            bool operationLayout = false;
            foreach (string x in operationR)
            {
                if (operands.Contains(x))
                {
                    operationLayout = true;
                }
            }
            if (operationLayout)
            {
                for (int i = 0; i < varnumber; i++)
                {
                    if (operands.Contains(varNames[i]))
                    {
                        operands = operands.Replace(varNames[i], varValues[i]);
                    }
                }
                bool res = (bool)dt.Compute(operands, "");
                return res;
            }
            else
            {
                throw new InvalidOperationException("No Comparator for Operation");
            }
        }
    }
}
