﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace c3528628
{
    public class DrawTools
    {
        private Bitmap drawZone;
        private int penX = 0;
        private int penY = 0;
        private PictureBox outputArea;
        private Graphics vibe;
        private Pen pen = new Pen(Color.Black);
        private SolidBrush brush = new SolidBrush(Color.Black);
        private RichTextBox errorReporter;
        //Creates required variables for handling drawing
        /*
         * Sets the location for error reporting
         */
        public void setEReporter(RichTextBox tBox)
        {
            errorReporter = tBox;
        }
        /*
         * Sets the size of the drawing area by getting the dimensitons of the inputted picture
         * box and binds the graphics area to the picturebox
         */
        public void setArea(PictureBox input)
        {
            drawZone = new Bitmap(input.Size.Width, input.Size.Height);
            vibe = Graphics.FromImage(drawZone);
            outputArea = input;
        }
        /*
         * A command to set the draw pointer locaiton in the graphics area
         */
        public void setPos(int x, int y)
        {
            penX = x;
            penY = y;
        }
        /*
         * Command to set the colour of the pen during drawing
         */
        public void setColor(string c)
        {
            Color checkColor = Color.FromArgb(0, 0, 0, 0);
            Color tempColor = Color.FromName(c);
            if (tempColor == checkColor)
            {
                errorReporter.AppendText("\' " + c + "\' is not a valid color\n");
            }
            else
            {
                brush.Color = tempColor;
                pen.Color = tempColor;
            }
            //The system attempts to generate a colour from the inputted Colour name,
            //with a colour with ARGB values of 0,0,0,0 being generated on invalid colours
        }
        /*
         * Function to draw from the pointers current location to the inputted location#
         * leaving the pointer at the inputted location
         */
        public void drawTo(int x, int y)
        {
            vibe.DrawLine(pen, penX, penY, x, y);
            setPos(x, y);
            outputArea.Image = drawZone;
        }
        /*
         * Draws a rectangle via inputted height and width, from the top left corner
         */
        public void drawRect(int w, int h)
        {
            vibe.DrawRectangle(pen, penX, penY, w, h);
            outputArea.Image = drawZone;
        }
        /*
         * Draws a circle from the inputted radius, centred around the current pointer locaiton
         */
        public void drawCirc(int r)
        {
            vibe.DrawEllipse(pen, (penX - (r / 2)), (penY - (r / 2)), r, r);
            outputArea.Image = drawZone;
        }
        /*
         * Draws a triangle using the current pointer location and two more sets of inputted co-ordinates
         */
        public void drawTri(int x1, int y1, int x2, int y2)
        {
            PointF pointA = new PointF(penX, penY);
            PointF pointB = new PointF(x1, y1);
            PointF pointC = new PointF(x2, y2);
            PointF[] triPoints =
                {
                pointA,
                pointB,
                pointC
                };
            vibe.DrawPolygon(pen, triPoints);
            outputArea.Image = drawZone;
        }
        /*
         * Draws a filled rectangle from the current coordinates based on inputted width and height from it's top-left corner
         */
        public void drawFillRect(int w, int h)
        {
            vibe.FillRectangle(brush, penX, penY, w, h);
            outputArea.Image = drawZone;
        }
        /*
         * Draws a filled circle centred around the current pointer location with the given raidus
         */
        public void drawFillCirc(int r)
        {
            vibe.FillEllipse(brush, (penX - (r / 2)), (penY - (r / 2)), r, r);
            outputArea.Image = drawZone;
        }
        /*
         * Draws a filled triangle using the current co-ordinates of the pointer and the two passed co-ordinates
         */
        public void drawFillTri(int x1, int y1, int x2, int y2)
        {
            PointF pointA = new PointF(penX, penY);
            PointF pointB = new PointF(x1, y1);
            PointF pointC = new PointF(x2, y2);
            PointF[] triPoints =
                {
                pointA,
                pointB,
                pointC
                };
            vibe.FillPolygon(brush, triPoints);
            outputArea.Image = drawZone;
        }
        /*
         * Clears the drawing area
         */
        public void clear()
        {
            vibe.Clear(Color.White);
            outputArea.Image = drawZone;
        }
    }
}
