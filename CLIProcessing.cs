﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c3528628
{
    public class CLIProcessing
    {
        private DrawTools drawHandler = new DrawTools();
        private mathHandler mathHandler = new mathHandler();
        private RichTextBox console;
        private Boolean fill = false;
        private List<int> whileStarts = new List<int>();
        private bool readState = true;
        private int nestIfNo = 0;
        private int nestWhileNo = 0;
        private bool returnToWhile = false;
        //Initializes drawing handler and verification variables
        /*
         * Function to handle location for error reporting; including passing to the drawing handler
         */
        public void setConsole(RichTextBox inputBox)
        {
            console = inputBox;
            drawHandler.setEReporter(inputBox);
        }
        public bool getLineUpdate()
        {
            return returnToWhile;
        }
        public int getLastWhile()
        {
            return whileStarts[whileStarts.Count - 1];
        }
        /*
         * Function for passing the drawing area to the drawing handler
         */
        public void setGraphics(PictureBox input)
        {
            drawHandler.setArea(input);
        }
        /*
         * Function to set the active fill value for drawing
         */
        private void setFill(bool t)
        {
            if (t)
            {
                fill = true;
            }
            else
            {
                fill = false;
            }
        }
        /*
         * Comprehensive case command to manage possible inputs from the user to process drawing
         */
        public void runCommand(String[] currentCommand, int line)
        {
            returnToWhile = false;
            int commandLen = currentCommand.Length;
            if (readState)
            {
                switch (currentCommand[0].ToLower())
                {
                    case "reset":
                        if (commandLen == 1)
                        {
                            drawHandler.setPos(0, 0);
                        }
                        else
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Command passed Parameters\n");
                        }
                        //Handles correct syntax for the reset command which resets the location of the pointer and has no parameters
                        break;
                    case "clear":
                        if (commandLen == 1)
                        {
                            drawHandler.clear();
                        }
                        else
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Command passed Parameters\n");
                        }
                        //Handles the correct syntax for the clear command which clears the drawing area and has no parameters
                        break;
                    case "circle":
                        if (commandLen == 2)
                        {
                            if (mathHandler.paramCheck(currentCommand[1]))
                            {
                                if (fill)
                                {
                                    drawHandler.drawFillCirc(mathHandler.paramHandle(currentCommand[1]));
                                }
                                else
                                {
                                    drawHandler.drawCirc(mathHandler.paramHandle(currentCommand[1]));
                                }
                            }
                            else
                            {
                                console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer Radius\n");
                            }
                        }
                        else
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Number of Parameters\n");
                        }
                        break;
                    case "pen":
                        if (commandLen == 2)
                        {
                            drawHandler.setColor(currentCommand[1]);
                        }
                        else if (commandLen == 4)
                        {
                            if (currentCommand[1].ToLower() == "draw")
                            {
                                if (mathHandler.paramCheck(currentCommand[2]) && mathHandler.paramCheck(currentCommand[3]))
                                {
                                    drawHandler.drawTo(mathHandler.paramHandle(currentCommand[2]), mathHandler.paramHandle(currentCommand[3]));
                                }
                                else if (mathHandler.paramCheck(currentCommand[3]))
                                {
                                    console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer X Position\n");
                                }
                                else if (mathHandler.paramCheck(currentCommand[2]))
                                {
                                    console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer Y Position\n");
                                }
                                else
                                {
                                    console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer Coordinates\n");
                                }
                            }
                            else
                            {
                                console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Command\n");
                            }
                        }
                        else
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Number of Parameters\n");
                        }
                        break;
                    case "fill":
                        if (commandLen == 2)
                        {
                            switch (currentCommand[1].ToLower())
                            {

                                case "true":
                                case "on":
                                    setFill(true);
                                    break;
                                case "false":
                                case "off":
                                    setFill(false);
                                    break;
                                default:
                                    console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\' has invalid parameters, please ensure to pass a valid integer\n");
                                    break;
                            }
                        }
                        else
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Number of Parameters\n");
                        }
                        break;
                    case "rectangle":
                        if (commandLen == 3)
                        {
                            if (mathHandler.paramCheck(currentCommand[1]) && mathHandler.paramCheck(currentCommand[2]))
                            {
                                if (fill)
                                {
                                    drawHandler.drawFillRect(mathHandler.paramHandle(currentCommand[1]), mathHandler.paramHandle(currentCommand[2]));
                                }
                                else
                                {
                                    drawHandler.drawRect(mathHandler.paramHandle(currentCommand[1]), mathHandler.paramHandle(currentCommand[2]));
                                }
                            }
                            else if (mathHandler.paramCheck(currentCommand[2]))
                            {
                                console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer Width\n");
                            }
                            else if (mathHandler.paramCheck(currentCommand[1]))
                            {
                                console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer Height\n");
                            }
                            else
                            {
                                console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer Parameters\n");
                            }
                        }
                        else
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Number of Parameters\n");
                        }
                        break;
                    case "position":
                        if (commandLen == 4)
                        {
                            if (currentCommand[1].ToLower() == "pen")
                            {
                                if (mathHandler.paramCheck(currentCommand[2]) && mathHandler.paramCheck(currentCommand[3]))
                                {
                                    drawHandler.setPos(mathHandler.paramHandle(currentCommand[2]), mathHandler.paramHandle(currentCommand[3]));
                                }
                                else if (mathHandler.paramCheck(currentCommand[3]))
                                {
                                    console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer X Position\n");
                                }
                                else if (mathHandler.paramCheck(currentCommand[2]))
                                {
                                    console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer Y Position\n");
                                }
                                else
                                {
                                    console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Integer Coordinates\n");
                                }
                            }
                            else
                            {
                                console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Command\n");
                            }
                        }
                        else
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Number of Parameters\n");
                        }
                        break;


                    case "triangle":
                    case "tri":
                        if (commandLen == 5)
                        {
                            bool valicCoord1 = false;
                            bool validCoord2 = false;
                            if (!mathHandler.paramCheck(currentCommand[1]) && !mathHandler.paramCheck(currentCommand[2]) && !mathHandler.paramCheck(currentCommand[3]) && !mathHandler.paramCheck(currentCommand[4]))
                            {
                                console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Coordinate Parameters\n");
                            }
                            else
                            {
                                if (mathHandler.paramCheck(currentCommand[1]) && mathHandler.paramCheck(currentCommand[2]))
                                {
                                    valicCoord1 = true;
                                }
                                else
                                {
                                    console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Coordinate One\n");
                                }
                                if (mathHandler.paramCheck(currentCommand[3]) && mathHandler.paramCheck(currentCommand[4]))
                                {
                                    validCoord2 = true;
                                }
                                else
                                {
                                    console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Coordinate One\n");
                                }
                                if (valicCoord1 && validCoord2)
                                {
                                    if (fill)
                                    {
                                        drawHandler.drawFillTri(mathHandler.paramHandle(currentCommand[1]), mathHandler.paramHandle(currentCommand[2]), mathHandler.paramHandle(currentCommand[3]), mathHandler.paramHandle(currentCommand[4]));
                                    }
                                    else
                                    {
                                        drawHandler.drawTri(mathHandler.paramHandle(currentCommand[1]), mathHandler.paramHandle(currentCommand[2]), mathHandler.paramHandle(currentCommand[3]), mathHandler.paramHandle(currentCommand[4]));
                                    }
                                }
                            }

                        }
                        else
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Number of Parameters\n");
                        }
                        break;
                    case "if":
                        try
                        {
                            if (!mathHandler.operation(String.Join(" ", currentCommand, 1, (currentCommand.Length - 1))))
                            {
                                readState = false;
                                nestIfNo = 1;
                            }
                        }
                        catch (InvalidOperationException)
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Operators");
                        }
                        break;
                    case "while":
                        returnToWhile = false;
                        try
                        {
                            if (!whileStarts.Contains(line))
                            {
                                whileStarts.Add(line);
                            }
                            if (!mathHandler.operation(String.Join(" ", currentCommand, 1, (currentCommand.Length - 1))))
                            {
                                readState = false;
                                nestWhileNo++;
                            }
                        }
                        catch (InvalidOperationException)
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Operators");
                        }
                        break;
                    case "endwhile":
                        returnToWhile = true;
                        break;
                    case "endif":
                        break;
                    default:
                        if (currentCommand[1] == "=")
                        {
                            if (!mathHandler.intCheck(currentCommand[0]))
                            {
                                if (commandLen == 3)
                                {
                                    mathHandler.setVar(currentCommand[0], currentCommand[2]);
                                }
                                else
                                {
                                    String equation = String.Join(" ", currentCommand, 2, (currentCommand.Length - 2));
                                    mathHandler.setVar(currentCommand[0], mathHandler.MathEval(equation));
                                }
                                console.AppendText(mathHandler.getVarVal(currentCommand[0]).ToString());
                            }
                            else
                            {
                                console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Variables Cannot Have Integer Names");
                            }

                        }
                        else
                        {
                            console.AppendText("Line " + (line + 1) + ": \'" + String.Join(" ", currentCommand) + "\': Invalid Command\n");
                        }
                        break;
                }
            }else if (currentCommand[0].Equals("endif"))
            {
                nestIfNo--;
            }else if (currentCommand[0].Equals("if"))
            {
                nestIfNo++;
            }else if (currentCommand[0].Equals("while"))
            {
                whileStarts.Add(line);
                nestWhileNo++;
            }
            else if (currentCommand[0].Equals("endwhile"))
            {
                whileStarts.RemoveAt(whileStarts.Count -  1);
                nestWhileNo--;
            }
            if(nestIfNo == 0 && nestWhileNo == 0)
            {
                readState = true;
            }
        }
        /*
         * Function to verify the inputted string can be parsed to and integer
         */
       
    }
}
